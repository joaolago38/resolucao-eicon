package com.example.eicon;

import br.com.eicon.entidades.Pedido;
import br.com.eicon.repository.PedidoRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@DataJpaTest
public class PedidoRepositoryTest {
    @Autowired
    private PedidoRepository pedidoRepository;
    @AfterEach
    void clearUp() {
        pedidoRepository.deleteAll();
    }


    @Test
    void shouldReturnPedidos() {
        List<Pedido> pedidos = Arrays.asList(
                Pedido.builder().id(1).numeroControle(23).dataCadastrado(LocalDateTime.now()).codigoCliente(23).nomeProduto("arroz").quantidade(23).valor(234).build(),
                Pedido.builder().id(2).numeroControle(24).dataCadastrado(LocalDateTime.now()).codigoCliente(25).nomeProduto("batata").quantidade(24).valor(245).build(),
                Pedido.builder().id(3).numeroControle(25).dataCadastrado(LocalDateTime.now()).codigoCliente(27).nomeProduto("feijao").quantidade(25).valor(234).build()
        );
        pedidoRepository.saveAll(pedidos);

        Assertions.assertThat(pedidoRepository.findAll())
                .isNotNull()
                .hasSameSizeAs(pedidos);
    }
    @Test
    void shouldSaveCategoryWithValidData() {
     Pedido pedido =    Pedido.builder().id(1).numeroControle(23).dataCadastrado(LocalDateTime.now()).codigoCliente(23).nomeProduto("arroz").quantidade(23).valor(234).build();
        Pedido salvoPedido = pedidoRepository.save(pedido);
        Assertions.assertThat(salvoPedido)
                .isNotNull()
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(pedido);
    }

    @Test
    void shouldReturnCategoryWhenValidId() {
        Pedido pedido =    Pedido.builder().id(1).numeroControle(23).dataCadastrado(LocalDateTime.now()).codigoCliente(23).nomeProduto("arroz").quantidade(23).valor(234).build();

        Assertions.assertThat(pedido)
                .isNotNull();
        Assertions.assertThat(pedidoRepository.findById(pedido.getId()))
                .isNotNull()
                .containsInstanceOf(Pedido.class)
                .get()
                .hasFieldOrProperty("id")
                .hasFieldOrProperty("numeroControle")
                .hasFieldOrProperty("codigoCliente")
                .hasFieldOrProperty("nomeProduto")
                .hasFieldOrProperty("quantidade")
                 .hasFieldOrProperty("valor");
    }

    @Test
    void shouldReturnNullWhenNotFoundId() {
        Assertions.assertThat(pedidoRepository.findById(23))
                .isNotPresent()
                .isEmpty();
    }
}
