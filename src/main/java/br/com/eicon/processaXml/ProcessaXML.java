package br.com.eicon.processaXml;


import br.com.eicon.entidades.Pedido;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.time.LocalDateTime;

public class ProcessaXML {
    private static String converter(Document document) throws TransformerException {
        Transformer transformer =
                TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(document);
        transformer.transform(source, result);
        String xmlString = result.getWriter().toString();
        System.out.append(xmlString);
        return xmlString;
    }

        public static void gerarXml(Pedido pedido) throws Exception {
        //Criamos um nova instancia de DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        //Usamos o metodo newDocumentBuilder() para
        //criar um nova instancia de DocumentBuilder
        DocumentBuilder db = dbf.newDocumentBuilder();
        //Obtem uma nova instancia de um Document
        //objeto para construir uma arvore com Document.
        Document doc = db.newDocument();

        //Cria a tag raiz do XML pedido
        Element tagPedido = doc.createElement("Pedido");
        Element idPedido = doc.createElement("id");
        Element nomeControle = doc.createElement("numeroControle");
        Element dataCadastrado = doc.createElement("dataCadastrado");
        Element nomeProduto = doc.createElement("nomeProduto");
        Element valor = doc.createElement("valor");
        Element quantidade = doc.createElement("quantidade");

        //Insere os valores nas tags de pedido
        idPedido.setTextContent(String.valueOf(pedido.getId()));
        nomeControle.setTextContent(String.valueOf(pedido.getNumeroControle()));
        dataCadastrado.setTextContent(String.valueOf(pedido.getNumeroControle()));
        if(pedido.getDataCadastrado() == null){
            pedido.setDataCadastrado(LocalDateTime.now());
        }
        valor.setTextContent(String.valueOf(pedido.getValor()));
        quantidade.setTextContent(String.valueOf(pedido.getQuantidade()));
        nomeProduto.setTextContent(String.valueOf(pedido.getNumeroControle()));

        //Insere na tag Pedido
        tagPedido.appendChild(idPedido);
        tagPedido.appendChild(nomeControle);
        tagPedido.appendChild(dataCadastrado);
        tagPedido.appendChild(valor);
        tagPedido.appendChild(quantidade);
        tagPedido.appendChild(nomeProduto);
        String arquivo = converter(doc);
        //vamos agora salvar o arquivo xml
        salvarArquivo(arquivo);

    }

    private static File file = new File("C:\\Users\\PC\\resoluçao-eicon\\resolucao-eicon\\src\\main\\resources\\pedido.xml");

    private static void salvarArquivo(String documento) throws Exception {
        FileOutputStream fos = new FileOutputStream(file, true);
        fos.write(documento.getBytes());
        fos.flush();
        fos.close();
    }
    public static Pedido lerXml() throws Exception, SAXException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        //Informamos qual arquivo xml vamos ler

        Document doc = db.parse(new InputSource(file.toString()));
        //Criamos um objeto Element que vai receber as informacoes de doc
        Element raiz = doc.getDocumentElement();

        //Vamos criar um objeto Pedido
        Pedido pedido = new Pedido();
        //Informamos qual tag vamos ler
        NodeList endList = raiz.getElementsByTagName("Pedido");
        Element endElement = (Element) endList.item(0);

        pedido.setId(Integer.parseInt(getChildTagValue(endElement, "id")));
        pedido.setNumeroControle(Integer.parseInt(getChildTagValue(endElement, "numeroControle")));
        pedido.setCodigoCliente(Integer.parseInt(getChildTagValue(endElement, "codigoCliente")));
        pedido.setQuantidade(Integer.parseInt(getChildTagValue(endElement, "Quantidade")));
        pedido.setValor(Integer.parseInt(getChildTagValue(endElement, "valor")));
        pedido.setNomeProduto(getChildTagValue(endElement, "nomeProduto"));

        return pedido;

    }
    public static String getChildTagValue(Element elem, String tagName) throws Exception {
        NodeList children = elem.getElementsByTagName(tagName);
        String result = null;

        if (children == null) {
            return result;
        }

        Element child = (Element) children.item(0);

        if (child == null) {
            return result;
        }

        result = child.getTextContent();

        return result;
    }

}
