package br.com.eicon.repository;

import br.com.eicon.entidades.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;


public interface PedidoRepository extends JpaRepository<Pedido, Integer> {

    @Query("SELECT t FROM Pedido t WHERE t.numeroControle = :numeroControle")
   List<Pedido> buscaNumeroControleCadastrado(@Param("numeroControle")Integer numeroControle);
    @Query("SELECT t FROM Pedido t WHERE t.dataCadastrado = :dataCadastrado")
    List<Pedido>  buscaTodosPorDataCadastrada(@Param("dataCadastrado")LocalDateTime dataCadastrado);

    @Query("SELECT t FROM Pedido t WHERE t.numeroControle = :numeroControle")
    List<Pedido> buscaTodosPorNumeroPedido(@Param("numeroControle")Integer  numeroControle);

}
