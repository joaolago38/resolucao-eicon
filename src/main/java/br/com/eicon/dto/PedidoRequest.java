package br.com.eicon.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonPropertyOrder({ "numeroControle","dataCadastrado","nomeProduto","valor","quantidade","codigoCliente"})
public class PedidoRequest {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("numeroControle")
    private Integer numeroControle;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("dataCadastrado")
    @JsonIgnoreProperties(ignoreUnknown = true)
    private LocalDateTime dataCadastrado;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("nomeProduto")
    private String nomeProduto;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("valor")
    private Integer valor;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("quantidade")
    private Integer quantidade;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("codigoCliente")
    private Integer codigoCliente;
}
