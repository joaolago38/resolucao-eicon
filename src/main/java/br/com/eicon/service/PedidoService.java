package br.com.eicon.service;

import br.com.eicon.entidades.Pedido;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PedidoService {
    List<Pedido> listaTodosPedidos();

    List<Pedido> listaTodosPedidosDataCadastrada(LocalDateTime dataCadastro);
    List<Pedido> listaTodosPedidosNumeroCadastrado(Integer numeroCadastrado);
    Pedido salvaTodosPedidos(Pedido Pedido);

    void deletaPedido(Integer id);
    Page<Pedido> buscaPedidosPaginacao(int pageNo, int pageSize, String sortField, String sortDirection);
}
