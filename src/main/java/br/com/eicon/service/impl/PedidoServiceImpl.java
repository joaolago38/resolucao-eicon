package br.com.eicon.service.impl;

import br.com.eicon.entidades.Pedido;
import br.com.eicon.excessoes.RegraNegocioException;
import br.com.eicon.repository.PedidoRepository;
import br.com.eicon.service.PedidoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
public class PedidoServiceImpl implements PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;
    final static Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
    @Override
    public List<Pedido> listaTodosPedidos() {
        logger.info("Listando todos os Pedidos.");
        return (List<Pedido>)
                pedidoRepository.findAll();
    }

    @Override
    public List<Pedido> listaTodosPedidosDataCadastrada(LocalDateTime dataCadastro) {
        logger.info("Listando todos os Pedidos por Data Cadastrada.");
        return pedidoRepository.buscaTodosPorDataCadastrada(dataCadastro);
    }

    @Override
    public List<Pedido> listaTodosPedidosNumeroCadastrado(Integer numeroCadastrado) {
        logger.info("Listando todos os Pedidos por Numero Cadastrado.");
        return pedidoRepository.buscaTodosPorNumeroPedido(numeroCadastrado);
    }

    @Override
    public Pedido salvaTodosPedidos(Pedido pedido) {
        validaQuantidade(pedido);
        validaDataCadastrada(pedido);
        this.validaNumeroCadstrado(pedido);
        this.calculaDescontoTotal(pedido);
        this.calculaValorTotalDoPedido(pedido);
        return pedidoRepository.save(pedido);
    }

    private static void validaQuantidade(Pedido pedido) {
        if(pedido.getQuantidade() == null){
            pedido.setQuantidade(1);
        }
    }

    private static void validaDataCadastrada(Pedido pedido) {

        if(pedido.getDataCadastrado() == null){
            pedido.setDataCadastrado(LocalDateTime.now());
        }
    }

    @Override
    public void deletaPedido(Integer id) {
        pedidoRepository.deleteById(id);
    }

    @Override
    public Page<Pedido> buscaPedidosPaginacao(int pageNo, int pageSize, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();

        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        return this.pedidoRepository.findAll(pageable);
    }

  void  validaNumeroCadstrado(Pedido pedido) {

        List<Pedido> valorNumeroControle = pedidoRepository.buscaNumeroControleCadastrado(pedido.getNumeroControle());
        if (!valorNumeroControle.isEmpty()) {
            throw new RegraNegocioException("Numero de Controle já Cadastrado");
        }

    }

     private Integer  calculaDescontoTotal(Pedido pedido){
         var valorTotal = 0;
         var valorComDesconto=0;
         var valorOriginal = pedido.getValor();

        if(pedido.getQuantidade() < 5){
            valorComDesconto = pedido.getValor() * 5/100;
            valorTotal = pedido.getValor() - valorComDesconto;
        }
         if(pedido.getQuantidade() < 10){
             valorComDesconto = pedido.getValor() * 10/100;
             valorTotal = pedido.getValor() - valorComDesconto;
         }
           return valorTotal;
     }

     private Integer calculaValorTotalDoPedido(Pedido pedido){
        Integer valorTotalDoPedido = 0;
        Integer valorComDesconto = this.calculaDescontoTotal(pedido);
        valorTotalDoPedido = pedido.getQuantidade() * pedido.getQuantidade();
        pedido.setValor(valorTotalDoPedido);
        return valorTotalDoPedido;
     }

}
