package br.com.eicon.service.impl;

import br.com.eicon.entidades.Pedido;
import br.com.eicon.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import static br.com.eicon.processaXml.ProcessaXML.gerarXml;
import static br.com.eicon.processaXml.ProcessaXML.lerXml;

public class ProcessaArquivoXML {

    @Autowired
    private PedidoRepository pedidoRepository;

    public static void main(String[] args) {
        Pedido pedido = new Pedido();
        pedido.setId(100);
        pedido.setCodigoCliente(123);
        pedido.setValor(123);
        pedido.setNomeProduto("Arroz");
        pedido.setNumeroControle(1234);

        try {
            gerarXml(pedido);
            Pedido c = lerXml();
            System.out.println(c.toString());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }

    }
}
