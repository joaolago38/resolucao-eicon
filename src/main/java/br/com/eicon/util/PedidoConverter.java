package br.com.eicon.util;

import br.com.eicon.dto.PedidoResponse;
import br.com.eicon.dto.PedidoRequest;
import br.com.eicon.entidades.Pedido;

public class PedidoConverter {
    public static PedidoResponse toPessoaResponse(Pedido pedido) {
        PedidoResponse pedidoResponse = new PedidoResponse();
        pedidoResponse.setCodigoCliente(pedido.getCodigoCliente());
        pedidoResponse.setDataCadastrado(pedido.getDataCadastrado());
        pedidoResponse.setNomeProduto(pedido.getNomeProduto());
        pedidoResponse.setCodigoCliente(pedido.getCodigoCliente());
        pedidoResponse.setQuantidade(pedido.getQuantidade());
        return pedidoResponse;
    }

    public static PedidoRequest toPedidoRequest(Pedido pedido) {
        PedidoRequest pedidoRequest = new PedidoRequest();
        pedidoRequest.setCodigoCliente(pedido.getCodigoCliente());
        pedidoRequest.setDataCadastrado(pedido.getDataCadastrado());
        pedidoRequest.setNomeProduto(pedido.getNomeProduto());
        pedidoRequest.setCodigoCliente(pedido.getCodigoCliente());
        pedidoRequest.setQuantidade(pedido.getQuantidade());
        return  pedidoRequest;
    }

    public static Pedido toPessoa(PedidoRequest pedidoRequest) {
        Pedido pedido = new Pedido();
        pedido.setCodigoCliente(pedidoRequest.getCodigoCliente());
        pedido.setDataCadastrado(pedidoRequest.getDataCadastrado());
        pedido.setNomeProduto(pedidoRequest.getNomeProduto());
        pedido.setCodigoCliente(pedidoRequest.getCodigoCliente());
        pedido.setQuantidade(pedidoRequest.getQuantidade());
        return pedido;
    }

}
