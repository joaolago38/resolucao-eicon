package br.com.eicon.entidades;


import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Builder
@Entity
@Table(name = "pedido")
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "numeroControle")
    private Integer numeroControle;

    @Column(name = "dataCadastrado")
    private LocalDateTime dataCadastrado;

    @Column(name = "nomeProduto")
    private String nomeProduto;

    @Column(name = "valor")
    private Integer valor;

    @Column(name = "quantidade")
    private Integer quantidade;

    @Column(name = "codigoCliente")
    private Integer codigoCliente;


    public Pedido(){

    }

    public Pedido(Integer id, Integer numeroControle, LocalDateTime dataCadastrado,
                  String nomeProduto, Integer valor,
                  Integer quantidade, Integer codigoCliente) {
        this.id = id;
        this.numeroControle = numeroControle;
        this.dataCadastrado = dataCadastrado;
        this.nomeProduto = nomeProduto;
        this.valor = valor;
        this.quantidade = quantidade;
        this.codigoCliente = codigoCliente;
    }
}
