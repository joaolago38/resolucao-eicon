package br.com.eicon.controller;

import br.com.eicon.entidades.Pedido;
import br.com.eicon.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;
    @PostMapping("/pedido")
    public Pedido salvaPedido(@Valid @RequestBody Pedido pedido)
    {
        return pedidoService.salvaTodosPedidos(pedido);
    }


    @GetMapping("/pedidos")
    public List<Pedido> buscaTodosOsPedidos()
    {
        return pedidoService.listaTodosPedidos();
    }

    @GetMapping("/pedidos/{data_cadastrada}")
    public List<Pedido> buscaTodosOsPedidosDataCadastro(@RequestParam(name = "dataCadastrado") LocalDateTime dataCadastrado)
    {
        return pedidoService.listaTodosPedidosDataCadastrada(dataCadastrado);
    }
    @GetMapping("/pedidos/{numero_pedidos}")
    public List<Pedido> buscaTodosOsPedidosNumeroPedidos(@RequestParam(name = "numeroControle") Integer numeroControle)
    {
        return pedidoService.listaTodosPedidosNumeroCadastrado(numeroControle);
    }

    @DeleteMapping("/pedidos/{id}")
    public String deletaPedidoPorId(@RequestParam("id")Integer Id)
    {
        pedidoService.deletaPedido(Id);
        return "Apagado com Sucesso";
    }
    @GetMapping("pedidos/pagina/{pageNo}")
    public String buscaPedidosPaginacao(@PathVariable (value = "pageNo") int pageNo,
                                @RequestParam("sortField") String sortField,
                                @RequestParam("sortDir") String sortDir,
                                Model model) {
        int pageSize = 5;

        Page<Pedido> page = pedidoService.buscaPedidosPaginacao(pageNo, pageSize, sortField, sortDir);
        List<Pedido> listEmployees = page.getContent();

        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());

        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

        model.addAttribute("listEmployees", listEmployees);
        return "index";
    }

}
