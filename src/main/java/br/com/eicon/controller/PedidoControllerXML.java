package br.com.eicon.controller;

import br.com.eicon.entidades.Pedido;
import br.com.eicon.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PedidoControllerXML {

    @Autowired
    private PedidoRepository pedidoRepository;
    @GetMapping("/pedido-xml/{id}")
    public ResponseEntity<Pedido> buscaPedidoXmlById(@PathVariable("id") Integer id) {
        Optional<Pedido> pedidoData = pedidoRepository.findById(id);

        if (pedidoData.isPresent()) {
            return new ResponseEntity<>(pedidoData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/pedido-xml")
    public ResponseEntity<Pedido> salvaPedidoXML(@RequestBody Pedido pedido) {
        try {
            Pedido pedidoData = pedidoRepository.save(new Pedido(pedido.getId(), pedido.getNumeroControle(),
                    pedido.getDataCadastrado(),pedido.getNomeProduto(), pedido.getValor(), pedido.getQuantidade(),pedido.getQuantidade()));
            return new ResponseEntity<>(pedidoData, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }


}








